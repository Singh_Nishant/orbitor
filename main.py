
from flask import Flask, render_template, request, json, redirect, url_for
#from flask.ext.mysql import MySQL
from flaskext.mysql import MySQL
import sys
from rasa_core.interpreter import RasaNLUInterpreter
from rasa_core.agent import Agent

app = Flask(__name__)
mysql = MySQL()
interpreter = RasaNLUInterpreter("models/nlu/default/current")
agent = Agent.load("models/dialogue", interpreter=interpreter)


# mysql Configurations
app.config['MYSQL_DATABASE_USER'] = 'root'
app.config['MYSQL_DATABASE_PASSWORD'] = 'Gurudev123'
app.config['MYSQL_DATABASE_DB'] = 'Orbitor'
app.config['MYSQL_DATABASE_HOST'] = 'localhost'
mysql.init_app(app)


@app.route('/')
def main():
    return render_template('home.html')


@app.route('/postPoem')
def postPoem():
    return render_template('post.html')


@app.route('/showSignIn')
def showSignIn():
    return render_template('signin.html')


# @app.route('/home')
# def home():
#     return render_template('home.html')


@app.route('/signIn',  methods=['POST', 'GET'])
def signIn():
    # read the posted values from the UI
    _email = request.form['inputEmail']
    _password = request.form['inputPassword']

   # validate the received values
    try:

        if _email and _password:
            print('All Good, lets call MySQL')
            conn = mysql.connect()
            cursor = conn.cursor()
            cursor.execute("SELECT COUNT(1) FROM tbl_user WHERE user_username = %s;",
                           [_email])  # CHECKS IF USERNAME EXSIST
        print(cursor.fetchone()[0])
        if cursor.fetchone()[0]:
            cursor.execute("SELECT user_password FROM tbl_user WHERE user_username = %s;",
                           [_email])  # FETCH THE HASHED PASSWORD
        for row in cursor.fetchall():
            print(row[0])
            if _password == row[0]:
                return json.dumps({'    Sucess': 'nishant'})
            else:
                return json.dumps({'    Failure': 'nishant'})

    except Exception as e:
        print(e)

    finally:
        cursor.close()
        conn.close()
        return "ok"


@app.route('/submitPoem',  methods=['POST', 'GET'])
def submitPoem():
    print("submitPoem() is called")
    # read the posted values from the UI
    _title = request.form['title']
    _author = request.form['author']
    _content = request.form['content']
    _date = request.form['date']

# validate the received values
    if _title and _author and _content and _date:
        print("Validation true and going for database")

      # All Good, let's call MySQL
        conn = mysql.connect()
        cursor = conn.cursor()
        cursor.callproc('sp_storePoem', (_title, _author, _content, _date))
        data = cursor.fetchall()

        if len(data) is 0:
            conn.commit()
            return json.dumps({'    Sucess': 'nishant'})
        else:
            return json.dumps({'    error': str(data[0])})
    else:
        return json.dumps({'html': ' <span> Enter the required fields</span>'})


@app.route('/getPoem', methods=['POST', 'GET'])
def getPoem():
    try:
        print("Inside Poem in python code")
        conn = mysql.connect()
        cursor = conn.cursor()
        cursor.execute("select * from poem")
        poems = cursor.fetchall()
        print("Executed")
        # print(poems)

        poem_dict = []
        for poem in poems:
            poem_dict_inner = {
                'Title': poem[0],
                'Auther_Name': poem[1],
                'Description': poem[2],
                'Date': poem[3]
            }
            poem_dict.append(poem_dict_inner)

        # print(poem_dict)
        poem_response={'results':poem_dict}
        return json.dumps(poem_response)

    except Exception as e:
        print(e)
        return render_template('error.html', error=str(e))




@app.route('/chatBot',  methods=['POST'])
def chatBot():
    """
    chatBot end point that performs NLU using rasa.ai
    and constructs response from response.py
    """
    print("chatbot() is called")
    print("user says    "+request.json['msgText'])
    botMessage=agent.handle_message(request.json['msgText'])
    print("Bot returned below message:")
    print(botMessage)
    message={'msgText':''.join(botMessage),'msgUser':'bot'}
    return json.dumps(message)


if __name__ == '__main__':
    app.run(debug=True)

$(function(){
	$('#btnSignIn').click(function(){
		console.log('Signin method called');
		$.ajax({
			url: '/signIn',
			data: $('form').serialize(),
			type: 'POST',
			success: function(response){
				console.log("Sign In Successful");
				window.location.href = "http://localhost:5000/home";
			},
			error: function(error){
				console.log(+error);
			}
		});
	});
});

    $(function() {
        console.log("I am inside poem JS");
            $.ajax({
                url: '/getPoem',
                type: 'GET',
                success: function(res) {
                    console.log(res);
                    var div = $('<div>')
                            .attr('class', 'list-group')
                            .append($('<a>')
                                  .attr('class', 'list-group-item active')
                                          .append($('<h4>')
                                               .attr('class', 'list-group-item-heading'),
                                                   $('<p>')
                                                       .attr('class', 'list-group-item-text')));

                    var poemObj = JSON.parse(res);
                    
                    var poemList=poemObj.results
                    console.log(JSON.stringify(poemList));
                    var poem = '';

                    function nl2br (str, is_xhtml) {
                        console.log("nl2br function is called");
                          var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
                          return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1'+ breakTag +'$2');
                        }

                    $.each(poemList, function(index, value) {
                        poem = $(div).clone();
                        $(poem).find('h4').html(value.Title);
                        $(poem).find('p').html(nl2br(value.Description));
                        $('.jumbotron').append(poem);
                        });

                },
                error: function(error) {
                    console.log(error);
                }
            });
        });

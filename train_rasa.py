from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import logging
from rasa_core.agent import Agent
from rasa_core.policies.memoization import MemoizationPolicy
from rasa_core.policies.keras_policy import KerasPolicy
from rasa_core.policies.memoization import MemoizationPolicy

class OrbitorPolicy(KerasPolicy):
    def model_architecture(self, num_features, num_actions, max_history_len):
        """Build a Keras model and return a compiled model."""
        from keras.layers import LSTM, Activation, Masking, Dense
        from keras.models import Sequential

        n_hidden = 80  # size of hidden layer in LSTM
        # Build Model
        model = Sequential()
        model.add(Masking(-1, batch_input_shape=(None, max_history_len, num_features)))
        model.add(LSTM(n_hidden, batch_input_shape=(None, max_history_len, num_features)))
        model.add(Dense(input_dim=n_hidden, output_dim=num_actions))
        model.add(Activation('softmax'))

        model.compile(loss='categorical_crossentropy',
                      optimizer='adam',
                      metrics=['accuracy'])

        #logger.debug(model.summary())
        return model




def train_rasa_dm():
    training_data_file = 'data/story.md'
    model_path = 'models/dialogue'
    domain_file='restaurant_domain.yml'

    agent = Agent(domain_file,
                  policies=[MemoizationPolicy(), OrbitorPolicy()])
    
    agent.train(
        training_data_file,
        max_history=41,
        epochs=100,
        batch_size=50,
        validation_split=0.2,
	    augmentation_factor=50
    )

    agent.persist(model_path)
    


if __name__ == '__main__':
    logging.basicConfig(level="DEBUG")
    train_rasa_dm()
